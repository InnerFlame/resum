<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
    <title>Concord</title>

    <link href="<?php echo Yii::app()->theme->baseUrl; ?>/css/main.css" rel="stylesheet" type="text/css" />
    <link href='http://fonts.googleapis.com/css?family=Cuprum' rel='stylesheet' type='text/css' />

    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js"></script>

    <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/plugins/spinner/jquery.mousewheel.js"></script>
    <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/plugins/spinner/ui.spinner.js"></script>

    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js"></script>

    <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/main.js"></script>

    <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/plugins/wysiwyg/jquery.wysiwyg.js"></script>
    <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/plugins/wysiwyg/wysiwyg.image.js"></script>
    <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/plugins/wysiwyg/wysiwyg.link.js"></script>
    <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/plugins/wysiwyg/wysiwyg.table.js"></script>

    <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/plugins/flot/jquery.flot.js"></script>
    <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/plugins/flot/jquery.flot.orderBars.js"></script>
    <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/plugins/flot/jquery.flot.pie.js"></script>
    <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/plugins/flot/excanvas.min.js"></script>
    <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/plugins/flot/jquery.flot.resize.js"></script>

    <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/plugins/tables/jquery.dataTables.js"></script>
    <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/plugins/tables/colResizable.min.js"></script>

    <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/plugins/forms/forms.js"></script>
    <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/plugins/forms/autogrowtextarea.js"></script>
    <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/plugins/forms/autotab.js"></script>
    <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/plugins/forms/jquery.validationEngine-en.js"></script>
    <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/plugins/forms/jquery.validationEngine.js"></script>
    <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/plugins/forms/jquery.dualListBox.js"></script>
    <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/plugins/forms/chosen.jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/plugins/forms/jquery.maskedinput.min.js"></script>
    <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/plugins/forms/jquery.inputlimiter.min.js"></script>
    <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/plugins/forms/jquery.tagsinput.min.js"></script>

    <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/plugins/other/calendar.min.js"></script>
    <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/plugins/other/elfinder.min.js"></script>

    <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/plugins/uploader/plupload.js"></script>
    <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/plugins/uploader/plupload.html5.js"></script>
    <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/plugins/uploader/plupload.html4.js"></script>
    <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/plugins/uploader/jquery.plupload.queue.js"></script>

    <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/plugins/ui/jquery.progress.js"></script>
    <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/plugins/ui/jquery.jgrowl.js"></script>
    <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/plugins/ui/jquery.tipsy.js"></script>
    <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/plugins/ui/jquery.alerts.js"></script>
    <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/plugins/ui/jquery.colorpicker.js"></script>

    <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/plugins/wizards/jquery.form.wizard.js"></script>
    <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/plugins/wizards/jquery.validate.js"></script>

    <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/plugins/ui/jquery.breadcrumbs.js"></script>
    <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/plugins/ui/jquery.collapsible.min.js"></script>
    <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/plugins/ui/jquery.ToTop.js"></script>
    <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/plugins/ui/jquery.listnav.js"></script>
    <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/plugins/ui/jquery.sourcerer.js"></script>
    <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/plugins/ui/jquery.timeentry.min.js"></script>
    <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/plugins/ui/jquery.prettyPhoto.js"></script>

    <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/custom.js"></script>

    <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/charts/chart.js"></script>

    <link rel="shortcut icon" href="<?php echo Yii::app()->theme->baseUrl; ?>/images/favicon/favicon.ico" type="image/x-icon" />


</head>

<body>

<!-- Top navigation bar -->
<div id="topNav">
    <div class="fixed">
        <div class="wrapper">
            <div class="welcome"><a href="#" title=""><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/userPic.png" alt="" /></a><span><?php
                    if(Yii::app()->user->checkAccess('1')){
                        echo 'Я, админ ';
                        echo Yii::app()->user->name;
                    }
                    if(Yii::app()->user->checkAccess('0')){
                        echo 'Я, пользователь ';
                        echo Yii::app()->user->name;
                    }
                    if(Yii::app()->user->isGuest){
                        echo 'Я, гость';
                    }

                    ?></span></div>
            <div class="userNav">
                <ul>
                    <li><a href="/site" title=""><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/icons/color/arrow-curve-180.png" alt="" /><span>Main site</span></a></li>
                    <li class="dd"><a title=""><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/icons/topnav/tasks.png" alt="" /><span>Other</span><span class="numberTop">2</span></a>
                        <ul class="menu_body">
                            <li><a href="#" title="" class="sInbox">Help</a></li>
                            <li><a href="#" title="" class="sOutbox">Contact admin</a></li>
                        </ul>
                    </li>

                    <?php if(Yii::app()->user->isGuest):?>
                        <li><a href="/user/create" title=""><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/icons/topnav/register.png" alt="" /><span>Registration</span></a></li>
                    <?php endif;?>

                    <?php if(Yii::app()->user->isGuest):?>
                        <li><a href="/site/login" title=""><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/icons/topnav/mainWebsite.png" alt="" /><span>Login</span></a></li>
                    <?php endif;?>

                    <?php if(!Yii::app()->user->isGuest):?>
                        <li><a href="/site/logout" title=""><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/icons/topnav/logout.png" alt="" /><span>Logout</span></a></li>
                    <?php endif;?>
                </ul>
            </div>
            <div class="fix"></div>
        </div>
    </div>
</div>

<!-- Header -->
<!--<div id="header" class="wrapper">-->
<!--    <div class="logo"><a href="/" title=""><img src="--><?php //echo Yii::app()->theme->baseUrl; ?><!--/images/concord-logo.png" alt="" /></a></div>-->
<!--    <div class="middleNav">-->
<!--        <ul>-->
<!--            <li class="iMes"><a href="#" title=""><span>Support tickets</span></a><span class="numberMiddle">9</span></li>-->
<!--            <li class="iStat"><a href="#" title=""><span>Statistics</span></a></li>-->
<!--            <li class="iUser"><a href="#" title=""><span>User list</span></a></li>-->
<!--            <li class="iOrders"><a href="#" title=""><span>Billing panel</span></a></li>-->
<!--        </ul>-->
<!--    </div>-->
<!--    <div class="fix"></div>-->
<!--</div>-->


<!-- Content wrapper -->
<div class="wrapper">

<!-- Left navigation -->
<div class="leftNav">
    <ul id="menu">
        <li class="dash"><a href="/site" title="" class="active"><span>Home</span></a></li>
        <li class="contacts"><a href="#" title="" class="exp"><span>Пользователи</span><span class="numberLeft">3</span></a>
            <ul class="sub">
                <li><a href="/user" title="">Список пользователей</a></li>
                <li><a href="/user/create" title="">Добавить пользователя</a></li>
                <li><a href="/user/view/<?php echo Yii::app()->user->id?>" title="">Профайл</a></li>
            </ul>
        </li>
    </ul>
</div>

<!-- Content -->
<div class="content">
    <?php echo $content; ?>
</div>

<!-- Footer -->
<div id="footer">
    <div class="wrapper">
        <span>&copy; Copyright 2014.</span>
    </div>
</div>

</body>
</html>

