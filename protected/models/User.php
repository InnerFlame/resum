<?php

/**
 * This is the model class for table "user_tbl".
 *
 * The followings are the available columns in table 'user_tbl':
 * @property integer $user_id
 * @property string $email
 * @property string $password
 * @property integer $role
 * @property integer $status
 * @property string $date_created
 * @property string $link
 *
 * The followings are the available model relations:
 * @property ProfileTbl[] $profileTbls
 */
class User extends CActiveRecord
{
    public $confirmPassword;

    const ROLE_GUEST = 'guest';
    const ROLE_AUTH = 'auth';
    const ROLE_ADMIN = 'admin';

    const STATUS_ACTIVE = 'active';
    const STATUS_BANNED = 'banned';


	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'user_tbl';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('email', 'required','message' => 'Введите email'),
            array('password', 'required','message' => 'Введите пароль'),
            array('email', 'unique','message' => 'Такой email уже использеутся'),
            array('email', 'email','message' => 'Введите корректный email'),
            array('password', 'match', 'pattern'=>'/^[a-z0-9_-]{3,20}$/', 'message' => 'Пароль должен содержать cтрочные и прописные латинские буквы, цифры'),
            array('confirmPassword', 'compare', 'compareAttribute'=>'password', 'message' => 'Подтвердите пароль.'),
			array('email, password', 'length', 'max'=>50),
            array('status', 'in', 'range'=>array_keys(self::getStatus())),
            array('role', 'in', 'range'=>array_keys(self::getRole())),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('user_id, email, password, role, status, date_created, link', 'safe', 'on'=>'search'),
		);
	}

    public static function getStatus() {
        return array(
            self::STATUS_ACTIVE=>'one',
            self::STATUS_BANNED=>'two',
        );
    }

    public static function getRole() {
        return array(
            self::ROLE_GUEST=>'one',
            self::ROLE_AUTH=>'two',
            self::ROLE_ADMIN=>'three',
        );
    }

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
//			'profileTbls' => array(self::HAS_MANY, 'ProfileTbl', 'user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'user_id' => 'User',
			'email' => 'Email',
			'password' => 'Password',
			'role' => 'Role',
			'status' => 'Status',
			'date_created' => 'Date Created',
			'link' => 'Link',
            'confirmPassword'=>'Confirm',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('role',$this->role);
		$criteria->compare('status',$this->status);
		$criteria->compare('date_created',$this->date_created,true);
		$criteria->compare('link',$this->link,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return User the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    public function beforeSave()
    {

        if(empty($this->password) && empty($this->repeat_password) && !empty($this->initialPassword))
            $this->password=$this->repeat_password=$this->initialPassword;

        if(parent::beforeSave()) {

            $this->password = md5('innerflame' . $this->password);

            return true;
        }
        else
            return false;
    }
}
