﻿--
-- Скрипт сгенерирован Devart dbForge Studio for MySQL, Версия 6.2.280.0
-- Домашняя страница продукта: http://www.devart.com/ru/dbforge/mysql/studio
-- Дата скрипта: 11.12.2014 16:45:22
-- Версия сервера: 5.5.38-log
-- Версия клиента: 4.1
--


-- 
-- Отключение внешних ключей
-- 
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;

-- 
-- Установить режим SQL (SQL mode)
-- 
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- 
-- Установка кодировки, с использованием которой клиент будет посылать запросы на сервер
--
SET NAMES 'utf8';

-- 
-- Установка базы данных по умолчанию
--
USE res;

--
-- Описание для таблицы expirience_tbl
--
DROP TABLE IF EXISTS expirience_tbl;
CREATE TABLE expirience_tbl (
  expirience_id INT(11) NOT NULL AUTO_INCREMENT,
  expirience_place VARCHAR(255) NOT NULL,
  expirience_date VARCHAR(255) NOT NULL,
  expirience_duties TEXT NOT NULL,
  PRIMARY KEY (expirience_id)
)
ENGINE = INNODB
AUTO_INCREMENT = 1
CHARACTER SET utf8
COLLATE utf8_general_ci
COMMENT = 'справочная таблица мест работы пользователя';

--
-- Описание для таблицы hobby_tbl
--
DROP TABLE IF EXISTS hobby_tbl;
CREATE TABLE hobby_tbl (
  hobby_id INT(11) NOT NULL AUTO_INCREMENT,
  hobby_title VARCHAR(255) NOT NULL,
  PRIMARY KEY (hobby_id)
)
ENGINE = INNODB
AUTO_INCREMENT = 1
CHARACTER SET utf8
COLLATE utf8_general_ci
COMMENT = 'справочная таблица увлечений пользователя';

--
-- Описание для таблицы phone_tbl
--
DROP TABLE IF EXISTS phone_tbl;
CREATE TABLE phone_tbl (
  phone_id INT(11) NOT NULL AUTO_INCREMENT,
  phone_number VARCHAR(255) NOT NULL,
  phone_type ENUM('home','mobile','work') NOT NULL,
  PRIMARY KEY (phone_id)
)
ENGINE = INNODB
AUTO_INCREMENT = 1
CHARACTER SET utf8
COLLATE utf8_general_ci
COMMENT = 'справочная таблица номеров пользователя';

--
-- Описание для таблицы user_tbl
--
DROP TABLE IF EXISTS user_tbl;
CREATE TABLE user_tbl (
  user_id INT(11) NOT NULL AUTO_INCREMENT COMMENT 'идентификатор пользователя',
  email VARCHAR(50) NOT NULL COMMENT 'електронная почта пользователя',
  password VARCHAR(50) NOT NULL COMMENT 'пароль пользователя md5',
  role ENUM('guest','auth','admin') NOT NULL COMMENT 'роль пользователя',
  status ENUM('banned','active') NOT NULL COMMENT 'блокировака пользователя',
  date_created TIMESTAMP DEFAULT CURRENT_TIMESTAMP COMMENT 'дата изменения записи',
  link VARCHAR(255) DEFAULT NULL,
  PRIMARY KEY (user_id),
  UNIQUE INDEX email (email)
)
ENGINE = INNODB
AUTO_INCREMENT = 18
AVG_ROW_LENGTH = 8192
CHARACTER SET utf8
COLLATE utf8_general_ci
COMMENT = 'справочная таблица пользователей';

--
-- Описание для таблицы profile_tbl
--
DROP TABLE IF EXISTS profile_tbl;
CREATE TABLE profile_tbl (
  profile_id INT(11) NOT NULL AUTO_INCREMENT,
  first_name VARCHAR(50) NOT NULL,
  last_name VARCHAR(50) NOT NULL,
  patronymic VARCHAR(50) NOT NULL,
  filaname VARCHAR(255) NOT NULL,
  user_id INT(11) NOT NULL,
  phone_id INT(11) NOT NULL,
  expirience_id INT(11) NOT NULL,
  hobby_id INT(11) NOT NULL,
  PRIMARY KEY (profile_id),
  CONSTRAINT FK_profile_tbl_expirience_tbl_expirience_id FOREIGN KEY (expirience_id)
    REFERENCES expirience_tbl(expirience_id) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT FK_profile_tbl_hobby_tbl_hobby_id FOREIGN KEY (hobby_id)
    REFERENCES hobby_tbl(hobby_id) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT FK_profile_tbl_phone_tbl_phone_id FOREIGN KEY (phone_id)
    REFERENCES phone_tbl(phone_id) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT FK_profile_tbl_user_tbl_user_id FOREIGN KEY (user_id)
    REFERENCES user_tbl(user_id) ON DELETE RESTRICT ON UPDATE RESTRICT
)
ENGINE = INNODB
AUTO_INCREMENT = 1
CHARACTER SET utf8
COLLATE utf8_general_ci
COMMENT = 'справочная таблица профилей пользователей';

-- 
-- Вывод данных для таблицы expirience_tbl
--

-- Таблица res.expirience_tbl не содержит данных

-- 
-- Вывод данных для таблицы hobby_tbl
--

-- Таблица res.hobby_tbl не содержит данных

-- 
-- Вывод данных для таблицы phone_tbl
--

-- Таблица res.phone_tbl не содержит данных

-- 
-- Вывод данных для таблицы user_tbl
--
INSERT INTO user_tbl VALUES
(17, 'fo4rever@mail.ru', '23babf89888ad86964ab12f8aea8aa61', 'guest', 'active', '2014-12-11 16:07:25', '446cc80e672893c92c101399a1d1a554');

-- 
-- Вывод данных для таблицы profile_tbl
--

-- Таблица res.profile_tbl не содержит данных

-- 
-- Восстановить предыдущий режим SQL (SQL mode)
-- 
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;

-- 
-- Включение внешних ключей
-- 
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;